const http = require('http');

const server = http.createServer((req, res, next) => {
	res.end('hello world');
});

server.listen(8082);

server.on('error', (e) => {
	if ( e.code === 'EADDRINUSE' ) {
		console.log('Address in use, retrying...');
		setTimeout(() => {
			server.close();
		}, 1000);
	}
});

function doSomething() {
	console.log('You don not pass!');
}